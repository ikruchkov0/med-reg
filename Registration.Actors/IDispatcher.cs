﻿using System.Threading.Tasks;
using Registration.Actions;
using Registration.Aggregates;
using Registration.Data.Projections;

namespace Registration.Actors
{
    public interface IDispatcher
    {
        Task InitAsync();

        Task SendAsync<TAction>(AggregateType aggregate, ICommand<TAction> command,
            ProjectionType? projectionType = null)
            where TAction : class, IAction;
    }
}
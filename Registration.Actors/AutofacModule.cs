﻿using System.Runtime.CompilerServices;
using Autofac;
using Registration.Actors.Common;

[assembly: InternalsVisibleTo("Registration.Actors.Tests")]

namespace Registration.Actors
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AggregateActor>().AsSelf();
            builder.RegisterType<ProjectionActor>().AsSelf();
            builder.RegisterType<ClusterNode>().AsSelf();
            builder.RegisterType<ClusterDispatcher>().As<IDispatcher>().SingleInstance();
        }
    }
}
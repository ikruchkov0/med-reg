﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Google.Protobuf;
using MessagePack.Resolvers;
using Microsoft.Extensions.Logging;
using Proto;
using Proto.Cluster;
using Proto.Cluster.Consul;
using Proto.Remote;
using Registration.Aggregates;
using Registration.Aggregates.Common;
using Registration.Data.Projections;
using Registration.Data.Projections.Journal;

namespace Registration.Actors.Common
{
    internal class MessagePackSerializer : ISerializer
    {
        public MessagePackSerializer()
        {
            MessagePack.MessagePackSerializer.SetDefaultResolver(ContractlessStandardResolver.Instance);
        }

        #region Implementation of ISerializer

        public ByteString Serialize(object obj)
        {
            var data = MessagePack.MessagePackSerializer.Serialize(obj);
            return ByteString.CopyFrom(data, 0, data.Length);
        }

        public object Deserialize(ByteString bytes, string typeName)
        {
            return MessagePack.MessagePackSerializer.Deserialize<object>(bytes.ToByteArray());
        }

        public string GetTypeName(object message)
        {
            return message.GetType().Name;
        }

        #endregion
    }

    public class ClusterNode
    {
        private readonly AggregateActor.Factory _aggregateActorFactory;
        private readonly IDictionary<AggregateType, IAggregateContext> _aggregates;
        private readonly ILoggerFactory _loggerFactory;
        private readonly ProjectionActor.Factory _projectionActorFactory;
        private readonly IDictionary<ProjectionMetadata, IProjection> _projections;

        public ClusterNode(ILoggerFactory loggerFactory,
            IEnumerable<Lazy<IAggregateContext, AggregateContextMetadata>> aggregates,
            IEnumerable<Lazy<IProjection, ProjectionMetadata>> projections,
            AggregateActor.Factory aggregateActorFactory,
            ProjectionActor.Factory projectionActorFactory)
        {
            _loggerFactory = loggerFactory;
            _aggregates = aggregates.ToDictionary(x => x.Metadata.Type, x => x.Value);
            _projections = projections.ToDictionary(x => x.Metadata, x => x.Value);
            _aggregateActorFactory = aggregateActorFactory;
            _projectionActorFactory = projectionActorFactory;
        }

        public void Start(string clusterName, int port)
        {
            Log.SetLoggerFactory(_loggerFactory);
            Serialization.RegisterSerializer(new MessagePackSerializer(), true);
            RegisterAggregates();
            RegisterProjections();
            Cluster.Start(clusterName, "127.0.0.1", port, new ConsulProvider(new ConsulProviderOptions()));
        }

        private void RegisterAggregates()
        {
            foreach (var aggregate in _aggregates)
            {
                var actorProps = Actor.FromProducer(() => _aggregateActorFactory(aggregate.Value));
                var actorKind = aggregate.Key.ToString();
                Remote.RegisterKnownKind(actorKind, actorProps);
            }
        }

        private void RegisterProjections()
        {
            foreach (var projection in _projections)
            {
                if (!_aggregates.TryGetValue(projection.Key.AggregateType, out var aggregate))
                    throw new Exception($"Aggregate with type {projection.Key} is not registered");
                var actorProps = Actor.FromProducer(() => _projectionActorFactory(aggregate, projection.Value));
                Remote.RegisterKnownKind(GetProjectionActorKind(projection.Key.Type), actorProps);
            }
        }

        public async Task StartProjectionAsync()
        {
            foreach (var projection in _projections)
            {
                var name = GetProjectionActorKind(projection.Key.Type);
                var kind = GetProjectionActorKind(projection.Key.Type);
                var (_, result) = await Cluster.GetAsync(name, kind);
                var attempts = 0;
                while (result != ResponseStatusCode.OK)
                {
                    // waiting for cluster start
                    await Task.Delay(TimeSpan.FromMilliseconds(100));
                    (_, result) = await Cluster.GetAsync(name, kind);
                    attempts++;
                    if (attempts > 5)
                        throw new Exception($"Unable to get actor {kind} with name {name}");
                }
            }
        }

        private string GetProjectionActorKind(ProjectionType projectionType)
        {
            return $"Projection_{projectionType}";
        }
    }
}
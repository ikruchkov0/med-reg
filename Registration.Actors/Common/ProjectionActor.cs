﻿using System;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Proto;
using Registration.Aggregates.Common;
using Registration.Data.Projections.Journal;
using Registration.Journals.Events;

namespace Registration.Actors.Common
{
    public class ProjectionActor : IActor
    {
        public delegate ProjectionActor Factory(IAggregateContext context, IProjection projection);

        private readonly IEventJournal _eventJournal;
        private readonly IProjection _projection;
        private IDisposable _subscription;

        public ProjectionActor(IAggregateContext context, IProjection projection)
        {
            _eventJournal = context.Events;
            _projection = projection;
        }

        #region Implementation of IActor

        public async Task ReceiveAsync(IContext context)
        {
            switch (context.Message)
            {
                case Started _:
                    await _eventJournal.EnsureJournalAsync();
                    await _projection.Journal.EnsureJournalAsync();
                    _subscription = _eventJournal.ListenAll(DateTime.MinValue).SubscribeOn(TaskPoolScheduler.Default)
                        .Subscribe(
                            entry => { context.Self.Tell(entry); });
                    break;
                case IEventJournalEntry entry:
                    await _projection.Project(entry);
                    break;
            }
        }

        #endregion
    }
}
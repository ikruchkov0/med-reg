﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Threading.Tasks;
using Proto;
using Proto.Cluster;
using Proto.Remote;
using Registration.Actions;
using Registration.Aggregates;
using Registration.Aggregates.Common;
using Registration.Data.Projections;
using Registration.Data.Projections.Journal;

namespace Registration.Actors.Common
{
    internal class ClusterDispatcher : IDispatcher
    {
        private readonly IDictionary<AggregateType, IAggregateContext> _aggregates;
        private readonly IDictionary<ProjectionType, IProjection> _projections;

        public ClusterDispatcher(
            IEnumerable<Lazy<IAggregateContext, AggregateContextMetadata>> aggregates,
            IEnumerable<Lazy<IProjection, ProjectionMetadata>> projections)
        {
            _aggregates = aggregates.ToDictionary(x => x.Metadata.Type, x => x.Value);
            _projections = projections.ToDictionary(x => x.Metadata.Type, x => x.Value);
        }

        public async Task InitAsync()
        {
            foreach (var aggregate in _aggregates.Values)
            {
                await aggregate.Errors.EnsureJournalAsync();
                await aggregate.Events.EnsureJournalAsync();
            }

            foreach (var projection in _projections.Values)
                await projection.Journal.EnsureJournalAsync();
        }

        public async Task SendAsync<TAction>(AggregateType aggregateType, ICommand<TAction> command,
            ProjectionType? projectionType = null)
            where TAction : class, IAction
        {
            if (!_aggregates.TryGetValue(aggregateType, out var aggregate))
                throw new Exception($"Unknown aggregate type {aggregateType}");
            var errors = aggregate.Errors;
            var events = aggregate.Events;
            var aggregateKind = aggregateType.ToString();

            var (clinic, result) = await GetActorAsync(command.AggregateKey, aggregateKind);
            var attempts = 0;
            while (result != ResponseStatusCode.OK)
            {
                (clinic, result) = await GetActorAsync(command.AggregateKey, aggregateKind);
                attempts++;
                if (attempts > 1)
                    throw new Exception(
                        $"Unable to get actor {aggregateKind} with aggregate key {command.AggregateKey}");
            }

            var errorTask = errors
                .Listen(command.TrackId)
                .SubscribeOn(TaskPoolScheduler.Default)
                .FirstOrDefaultAsync()
                .ToTask();

            var eventTask = events
                .Listen(command.AggregateKey, command.TrackId)
                .SubscribeOn(TaskPoolScheduler.Default)
                .FirstOrDefaultAsync()
                .ToTask();

            var projectionTask = Task.CompletedTask;
            if (projectionType.HasValue)
            {
                if (!_projections.TryGetValue(projectionType.Value, out var projection))
                    throw new Exception($"Unknown projection type {projectionType.Value}");
                projectionTask = projection.Journal
                    .Listen(projectionType.Value, command.TrackId)
                    .SubscribeOn(TaskPoolScheduler.Default)
                    .FirstOrDefaultAsync()
                    .ToTask();
                ;
            }

            clinic.Tell(command);

            var processingTask = Task.WhenAll(eventTask, projectionTask);
            await Task.WhenAny(processingTask, errorTask);
            if (errorTask.IsCompleted)
            {
                var error = errorTask.Result;
                throw error.Exception;
            }
        }

        protected Task<(PID, ResponseStatusCode)> GetActorAsync(string actorId, string actorKind)
        {
            return Cluster.GetAsync(actorId, actorKind);
        }
    }
}
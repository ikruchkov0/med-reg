﻿using System;
using System.Threading.Tasks;
using Proto;
using Registration.Actions;
using Registration.Aggregates.Common;

namespace Registration.Actors.Common
{
    public class AggregateActor : IActor
    {
        public delegate AggregateActor Factory(IAggregateContext context);

        private readonly IAggregateContext _aggregateContext;
        private readonly IErrorHandler _errorHandler;
        private readonly IStateStorage _stateStorage;

        private IState _state;

        public AggregateActor(
            IAggregateContext aggregateContext,
            SateStorageFactory stateStorageFactory,
            ErrorHandlerFactory errorHandlerFactory
        )
        {
            _aggregateContext = aggregateContext;
            _errorHandler = errorHandlerFactory(aggregateContext.Errors);
            _stateStorage = stateStorageFactory(aggregateContext.Events);
        }

        public async Task ReceiveAsync(IContext context)
        {
            switch (context.Message)
            {
                case Started _:
                    var aggregateKey = context.Self.Id;
                    var initialState = _aggregateContext.GetInitialState(aggregateKey);
                    _state = await _stateStorage.RestoreAsync(initialState);
                    break;
                case ICommand command:
                    try
                    {
                        var @event = _aggregateContext.CommandHandler.Handle(_state, command);
                        _state = await _stateStorage.ApplyAsync(_state, @event, command);
                    }
                    catch (Exception ex)
                    {
                        await _errorHandler.HandleAsync(ex);
                        context.Self.Tell(Stop.Instance);
                    }
                    break;
            }
        }
    }
}
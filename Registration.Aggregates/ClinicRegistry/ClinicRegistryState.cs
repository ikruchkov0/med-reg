﻿using System;
using System.Collections.Generic;
using Registration.Aggregates.Common.Implementations;
using Registration.Events.Clinics.ClinicRegistration;

namespace Registration.Aggregates.ClinicRegistry
{
    internal class ClinicRegistryState : BaseState<IClinicRegistrationEvent, ClinicRegistryState>
    {
        public ClinicRegistryState(string aggregateKey) : base(aggregateKey)
        {
            Clinics = new Dictionary<string, string>();
        }

        public IDictionary<string, string> Clinics { get; }

        #region Overrides of BaseState<IClinicEvent,ClinicState>

        protected override ClinicRegistryState ApplyEventInternal(IClinicRegistrationEvent @event)
        {
            switch (@event)
            {
                case ClinicRegistered register:
                    Clinics[register.ClinicName.ToUpperInvariant()] = register.ClinicAddress;
                    return this;
                default:
                    throw new Exception($"Unknown event {@event.GetType().Name} ");
            }
        }

        #endregion
    }
}
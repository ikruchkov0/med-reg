﻿using Autofac;
using Registration.Aggregates.Common;

namespace Registration.Aggregates.ClinicRegistry
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<ClinicRegistryState>()
                .AsSelf();
            builder
                .RegisterType<ClinicRegistryCommandHander>()
                .AsSelf();
            builder
                .RegisterType<ClinicRegistryContext>()
                .As<IAggregateContext>()
                .WithMetadata<AggregateContextMetadata>(m => m.For(am => am.Type, AggregateType.ClinicRegistry));
        }
    }
}
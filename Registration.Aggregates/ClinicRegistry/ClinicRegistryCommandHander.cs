﻿using System;
using Registration.Actions.Clinics.ClinicRegistration;
using Registration.Aggregates.Common.Implementations;
using Registration.Events.Clinics.ClinicRegistration;

namespace Registration.Aggregates.ClinicRegistry
{
    internal class ClinicRegistryCommandHander : BaseCommandHander<ClinicRegistryState, IClinicRegistrationAction,
        IClinicRegistrationEvent>
    {
        #region Implementation of BaseCommandHander

        protected override IClinicRegistrationEvent HandleAction(ClinicRegistryState state,
            IClinicRegistrationAction action)
        {
            switch (action)
            {
                case RegisterClinic register:
                    if (state.Clinics.TryGetValue(register.ClinicName.ToUpperInvariant(), out var _))
                        throw new Exception($"Clinic with name {register.ClinicName} already registered");
                    return new ClinicRegistered
                    {
                        Id = $"clinic_{state.Clinics.Values.Count}",
                        ClinicName = register.ClinicName,
                        ClinicAddress = register.ClinicAddress
                    };
                default:
                    throw new Exception($"Unknown action {action.GetType().Name}");
            }
        }

        #endregion
    }
}
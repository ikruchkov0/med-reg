﻿using Registration.Actions.Clinics.ClinicRegistration;
using Registration.Aggregates.Common.Implementations;
using Registration.Events.Clinics.ClinicRegistration;
using Registration.Journals.Errors;
using Registration.Journals.Events;

namespace Registration.Aggregates.ClinicRegistry
{
    internal class ClinicRegistryContext : BaseAggregateContext<ClinicRegistryState, IClinicRegistrationAction,
        IClinicRegistrationEvent, ClinicRegistryCommandHander>
    {
        public ClinicRegistryContext(BaseState<IClinicRegistrationEvent, ClinicRegistryState>.Factory stateFactory,
            ClinicRegistryCommandHander commandHandler, EventJournalFactory eventJournalFactory,
            ErrorJournalFactory errorJournalFactory) : base(stateFactory, commandHandler, eventJournalFactory,
            errorJournalFactory)
        {
        }

        #region Overrides of BaseAggregateContext<ClinicRegistryState,IClinicRegistrationAction,IClinicRegistrationEvent>

        public override AggregateType Type => AggregateType.ClinicRegistry;

        protected override string JournalName => "ClinicRegistry";

        #endregion
    }
}
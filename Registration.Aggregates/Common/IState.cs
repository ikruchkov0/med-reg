﻿using Registration.Events;

namespace Registration.Aggregates.Common
{
    public interface IState
    {
        string AggregateKey { get; }
        ulong SequenceNumber { get; }
        IState ApplyEvent(ulong sequenceNumber, IEvent @event);
    }
}
﻿using System;
using System.Threading.Tasks;
using Registration.Journals.Errors;

namespace Registration.Aggregates.Common
{
    public delegate IErrorHandler ErrorHandlerFactory(IErrorJournal errorJournal);

    public interface IErrorHandler
    {
        Task HandleAsync(Exception reason);
    }
}
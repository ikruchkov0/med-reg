﻿using System.Threading.Tasks;
using Registration.Journals.Errors;
using Registration.Journals.Events;

namespace Registration.Aggregates.Common
{
    public interface IAggregateContext
    {
        IErrorJournal Errors { get; }

        IEventJournal Events { get; }

        AggregateType Type { get; }

        ICommandHandler CommandHandler { get; }

        IState GetInitialState(string aggregateKey);

        Task InitAsync();
    }
}
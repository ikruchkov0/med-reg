﻿using System;
using System.Threading.Tasks;
using Registration.Actions;
using Registration.Events;
using Registration.Journals.Events;
using Registration.Journals.Implementations.Events;

namespace Registration.Aggregates.Common.Implementations
{
    internal class StateStorage : IStateStorage
    {
        private readonly Func<IEventJournalEntryBuilder> _entryBuilderFactory;
        private readonly IEventJournal _journal;

        public StateStorage(IEventJournal eventJournal, Func<IEventJournalEntryBuilder> entryBuilderFactory)
        {
            _journal = eventJournal;
            _entryBuilderFactory = entryBuilderFactory;
        }

        private IState ApplyEntry(IState state, IEventJournalEntry entry)
        {
            try
            {
                return state.ApplyEvent(entry.SequenceNumber, entry.Event);
            }
            catch (Exception ex)
            {
                throw new PersistenceException("Error while initializing state from store", ex, entry);
            }
        }

        #region Implementation of IStateStorage

        public async Task<IState> RestoreAsync(IState initialState)
        {
            var previousEntries = await _journal.QueryAsync(initialState.AggregateKey, initialState.SequenceNumber);
            var state = initialState;
            foreach (var entry in previousEntries)
                state = ApplyEntry(state, entry);
            return state;
        }

        public async Task<IState> ApplyAsync(IState state, IEvent @event, ICommand command)
        {
            var nextSequenceNumber = state.SequenceNumber + 1;
            var entry = _entryBuilderFactory()
                .WithEvent(@event)
                .WithAction(command.Action)
                .WithSequenceNumber(nextSequenceNumber)
                .WithAggregateKey(command.AggregateKey)
                .WithTrackId(command.TrackId)
                .ToEntry();
            var newState = ApplyEntry(state, entry);
            await _journal.PublishAsync(entry);
            return newState;
        }

        #endregion
    }
}
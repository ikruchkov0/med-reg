﻿using System;
using Registration.Actions;
using Registration.Events;

namespace Registration.Aggregates.Common.Implementations
{
    internal abstract class BaseCommandHander<TState, TAction, TEvent> : ICommandHandler
        where TAction : class, IAction
        where TState : BaseState<TEvent, TState>
        where TEvent : class, IEvent
    {
        #region Implementation of ICommandHandler

        public IEvent Handle(IState state, ICommand command)
        {
            if (!(state is TState typedState))
                throw new Exception($"Unknown state type {state.GetType().Name}");

            return HandleTyped(typedState, command);
        }

        #endregion

        protected TEvent HandleTyped(TState state, ICommand command)
        {
            try
            {
                if (!(command.Action is TAction typedAction))
                    throw new Exception($"Unknown action type {command.Action.GetType().Name}");
                return HandleAction(state, typedAction);
            }
            catch (Exception ex)
            {
                throw new ProcessingException("Error during command handling", ex, command);
            }
        }

        protected abstract TEvent HandleAction(TState state, TAction action);
    }
}
﻿using System.Threading.Tasks;
using Registration.Actions;
using Registration.Events;
using Registration.Journals.Errors;
using Registration.Journals.Events;

namespace Registration.Aggregates.Common.Implementations
{
    internal abstract class BaseAggregateContext<TState, TAction, TEvent, THandler> : IAggregateContext
        where TAction : class, IAction
        where TState : BaseState<TEvent, TState>
        where TEvent : class, IEvent
        where THandler : BaseCommandHander<TState, TAction, TEvent>
    {
        private readonly BaseState<TEvent, TState>.Factory _stateFactory;

        protected BaseAggregateContext(BaseState<TEvent, TState>.Factory stateFactory,
            THandler commandHandler,
            EventJournalFactory eventJournalFactory,
            ErrorJournalFactory errorJournalFactory)
        {
            _stateFactory = stateFactory;
            Events = eventJournalFactory(JournalName);
            Errors = errorJournalFactory(JournalName);
            CommandHandler = commandHandler;
        }

        protected abstract string JournalName { get; }

        #region Implementation of IAggregate<TState,in TEvent>

        public IErrorJournal Errors { get; }
        public IEventJournal Events { get; }
        public abstract AggregateType Type { get; }

        public IState GetInitialState(string aggregateKey)
        {
            return _stateFactory(aggregateKey);
        }

        public ICommandHandler CommandHandler { get; }

        public async Task InitAsync()
        {
            await Errors.EnsureJournalAsync();
            await Events.EnsureJournalAsync();
        }

        #endregion
    }
}
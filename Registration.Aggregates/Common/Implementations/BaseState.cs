﻿using System;
using Registration.Events;

namespace Registration.Aggregates.Common.Implementations
{
    internal abstract class BaseState<TEvent, TState> : IState
        where TEvent : class, IEvent
        where TState : BaseState<TEvent, TState>, IState
    {
        protected BaseState(string aggregateKey)
        {
            AggregateKey = aggregateKey;
        }

        protected abstract TState ApplyEventInternal(TEvent @event);

        protected TState ApplyTypedEvent(ulong sequenceNumber, TEvent @event)
        {
            var result = ApplyEventInternal(@event);
            SequenceNumber = sequenceNumber;
            return result;
        }

        internal delegate TState Factory(string aggregateKey);

        #region Implementation of IState

        public string AggregateKey { get; }
        public ulong SequenceNumber { get; private set; }

        public IState ApplyEvent(ulong sequenceNumber, IEvent @event)
        {
            switch (@event)
            {
                case TEvent typedEvent:
                    return ApplyTypedEvent(sequenceNumber, typedEvent);
                default:
                    throw new Exception($"Unknown type {@event.GetType().Name}");
            }
        }

        #endregion
    }
}
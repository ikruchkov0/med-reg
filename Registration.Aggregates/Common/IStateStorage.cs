﻿using System.Threading.Tasks;
using Registration.Actions;
using Registration.Events;
using Registration.Journals.Events;

namespace Registration.Aggregates.Common
{
    public delegate IStateStorage SateStorageFactory(IEventJournal eventJournal);

    public interface IStateStorage
    {
        Task<IState> RestoreAsync(IState initialState);

        /// <exception cref="PersistenceException"></exception>
        Task<IState> ApplyAsync(IState state, IEvent @event, ICommand command);
    }
}
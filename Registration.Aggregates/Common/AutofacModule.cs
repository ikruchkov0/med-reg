﻿using Autofac;
using Registration.Aggregates.Common.Implementations;

namespace Registration.Aggregates.Common
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ErrorHandler>().As<IErrorHandler>();
            builder.RegisterType<StateStorage>().As<IStateStorage>();
        }
    }
}
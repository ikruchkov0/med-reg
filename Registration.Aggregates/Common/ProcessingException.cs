﻿using System;
using Registration.Actions;

namespace Registration.Aggregates.Common
{
    public sealed class ProcessingException : Exception
    {
        public ProcessingException(string message, Exception innerException, ICommand<IAction> command)
            : base(message, innerException)
        {
            Command = command;
        }

        public ICommand<IAction> Command { get; set; }
    }
}
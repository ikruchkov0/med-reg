﻿using Registration.Actions;
using Registration.Events;

namespace Registration.Aggregates.Common
{
    public interface ICommandHandler
    {
        /// <exception cref="ProcessingException"></exception>
        IEvent Handle(IState state, ICommand command);
    }
}
﻿namespace Registration.Aggregates
{
    public class AggregateContextMetadata
    {
        public AggregateType Type { get; set; }
    }
}
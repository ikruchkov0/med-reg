﻿using System.Runtime.CompilerServices;
using Autofac;
using Registration.RethinkDb.Common.Implementation;

[assembly: InternalsVisibleTo("Registration.Journals.Tests")]

namespace Registration.RethinkDb.Common
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ConnectionProvider>().As<IRethinkConnectionProvider>()
                .OnActivated(args => args.Instance.Init()).SingleInstance().AutoActivate();
        }
    }
}
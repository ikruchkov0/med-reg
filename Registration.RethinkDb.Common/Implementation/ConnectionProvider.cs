﻿using Microsoft.Extensions.Logging;
using RethinkDb.Driver;
using RethinkDb.Driver.Net;
using RethinkDb.Driver.Utils;

namespace Registration.RethinkDb.Common.Implementation
{
    internal sealed class ConnectionProvider : IRethinkConnectionProvider
    {
        private static readonly RethinkDB R = RethinkDB.R;

        private static Connection _singleConnection;
        private readonly IRethinkDbConfiguration _configuration;

        private readonly ILogger<ConnectionProvider> _logger;

        public ConnectionProvider(ILoggerFactory loggerFactory, IRethinkDbConfiguration configuration)
        {
            _logger = loggerFactory.CreateLogger<ConnectionProvider>();
            _configuration = configuration;
        }

        public void Init()
        {
            if (_singleConnection != null && _singleConnection.Open)
                return;
            var connection = R.Connection()
                .Hostname(_configuration.Host)
                .Port(_configuration.Port)
                .Timeout(_configuration.TimeoutSeconds).Connect();

            if (!connection.Open) connection.Reconnect();

            connection.Use(_configuration.DatabaseName);
            connection.ConnectionError += (sender, exception) =>
            {
                _logger.LogError(0, exception, "Connection error");
            };
            connection.EnsureDatabaseAsync(_configuration.DatabaseName).WaitSync();
            _singleConnection = connection;
        }

        public Connection Connection => _singleConnection;
    }
}
﻿using RethinkDb.Driver;

namespace Registration.RethinkDb.Common
{
    public static class RethinkDbConfigurations
    {
        public static IRethinkDbConfiguration Default(string dbName)
        {
            return new RethinkDbConfigurationImpl
            {
                DatabaseName = dbName,
                Host = RethinkDBConstants.DefaultHostname,
                Port = RethinkDBConstants.DefaultPort,
                TimeoutSeconds = RethinkDBConstants.DefaultTimeout
            };
        }

        internal class RethinkDbConfigurationImpl : IRethinkDbConfiguration
        {
            public string DatabaseName { get; internal set; }

            public string Host { get; internal set; }

            public int Port { get; internal set; }

            public int TimeoutSeconds { get; internal set; }
        }
    }
}
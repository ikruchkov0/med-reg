﻿using System;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Threading.Tasks;
using FluentAssertions;
using Registration.Journals.Errors;
using Registration.Journals.Implementations.Errors;
using Xbehave;
using Xunit;

namespace Registration.Journals.Tests
{
    [Collection(nameof(RethinkDbCollection))]
    public class ErrorJournalTests
    {
        private readonly RethinkDbFixture _fixture;

        private IErrorJournal _journal;

        public ErrorJournalTests(RethinkDbFixture fixture)
        {
            _fixture = fixture;
        }

        [Background]
        public void Background()
        {
            "Create journal".x(
                async () =>
                {
                    var feed = new ErrorJournal(nameof(ErrorJournalTests), _fixture.ConnectionProvider);
                    _journal = await feed.EnsureJournalAsync();
                });
        }

        [Scenario]
        public void ListeningLiveItems(
            IActionErrorJournalEntry publishedJournalEntry,
            IObservable<IErrorJournalEntry> subscription,
            IErrorJournalEntry receivedJournalEntry)
        {
            var trackId = Guid.NewGuid().ToString();

            const string aggregateKey = nameof(ListeningLiveItems);
            "Subscribe on journal with track id".x(
                () => { subscription = _journal.Listen(trackId); });

            "Publish test error".x(
                async () =>
                {
                    publishedJournalEntry = ActionEntryFactory(aggregateKey, trackId);
                    await _journal.PublishActionAsync(publishedJournalEntry);
                });

            "Wait for envelope from subscription".x(
                () =>
                {
                    var receiveTask = subscription.FirstOrDefaultAsync().ToTask();
                    WaitWithTimeout(receiveTask, TimeSpan.FromSeconds(1));
                    receivedJournalEntry = receiveTask.Result;
                });

            "Received envelope should be equivalent to published".x(
                () =>
                {
                    receivedJournalEntry.Should().NotBeNull();
                    receivedJournalEntry.Should().BeEquivalentTo(
                        publishedJournalEntry,
                        opts => opts.Excluding(x => x.TimestampUtc));
                });
        }

        [Scenario]
        public void ListeningOldItems(
            IEventErrorJournalEntry publishedJournalEntry,
            IObservable<IErrorJournalEntry> subscription,
            IErrorJournalEntry receivedJournalEntry)
        {
            var trackId = Guid.NewGuid().ToString();
            const string aggregateKey = nameof(ListeningOldItems);
            "Publish test error".x(
                async () =>
                {
                    publishedJournalEntry = EventEntryFactory(aggregateKey, trackId);
                    await _journal.PublishEventAsync(publishedJournalEntry);
                });

            "Subscribe on journal with correlation id".x(() => { subscription = _journal.Listen(trackId); });

            "Wait for envelope from subscription".x(
                () =>
                {
                    var receiveTask = subscription.FirstOrDefaultAsync().ToTask();
                    WaitWithTimeout(receiveTask, TimeSpan.FromSeconds(1));
                    receivedJournalEntry = receiveTask.Result;
                });

            "Received envelope should be equivalent to published".x(
                () =>
                {
                    receivedJournalEntry.Should().BeEquivalentTo(
                        publishedJournalEntry,
                        opts => opts.Excluding(x => x.TimestampUtc));
                });
        }

        [Scenario]
        public void PublishingAction()
        {
            "Publish test action error".x(
                async () =>
                {
                    var trackId = Guid.NewGuid().ToString();
                    var aggregateKey = Guid.NewGuid().ToString();
                    var entry = ActionEntryFactory(aggregateKey, trackId);
                    await _journal.PublishActionAsync(entry);
                });
        }

        [Scenario]
        public void PublishingEvent()
        {
            "Publish test event error".x(
                async () =>
                {
                    var trackId = Guid.NewGuid().ToString();
                    var aggregateKey = Guid.NewGuid().ToString();
                    var entry = EventEntryFactory(aggregateKey, trackId);
                    await _journal.PublishEventAsync(entry);
                });
        }

        private static IActionErrorJournalEntry ActionEntryFactory(string aggregateKey, string trackId)
        {
            return new ActionErrorJournalEntryBuilder()
                .WithAggregateKey(aggregateKey)
                .WithTrackId(Guid.NewGuid().ToString())
                .WithAction(new TestAction())
                .WithException(new Exception(aggregateKey + trackId))
                .WithTrackId(trackId)
                .ToEntry();
        }

        private static IEventErrorJournalEntry EventEntryFactory(string aggregateKey, string trackId)
        {
            return new EventErrorJournalEntryBuilder()
                .WithAggregateKey(aggregateKey)
                .WithTrackId(Guid.NewGuid().ToString())
                .WithEvent(new TestEvent())
                .WithException(new Exception(aggregateKey + trackId))
                .WithTrackId(trackId)
                .ToEntry();
        }

        private static void WaitWithTimeout<T>(Task<T> taskToWait, TimeSpan timeout)
        {
            var executed = taskToWait.Wait(timeout);
            if (executed) return;

            throw new TimeoutException();
        }
    }
}
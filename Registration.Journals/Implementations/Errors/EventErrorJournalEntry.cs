using Registration.Events;
using Registration.Journals.Errors;

namespace Registration.Journals.Implementations.Errors
{
    internal class EventErrorJournalEntry : ErrorJournalEntry, IEventErrorJournalEntry
    {
        #region Implementation of IEventErrorJournalEntry

        public IEvent Event { get; set; }

        #endregion
    }

    public interface
        IEventErrorJournalEntryBuilder : IErrorJournalEntryBuilder<IEventErrorJournalEntry,
            IEventErrorJournalEntryBuilder>
    {
        IEventErrorJournalEntryBuilder WithEvent(IEvent @event);
    }

    internal class EventErrorJournalEntryBuilder :
        ErrorJournalEntryBuilder<IEventErrorJournalEntry, EventErrorJournalEntry, IEventErrorJournalEntryBuilder>,
        IEventErrorJournalEntryBuilder
    {
        #region Implementation of IEventErrorJournalEntryBuilder

        public IEventErrorJournalEntryBuilder WithEvent(IEvent @event)
        {
            Entry.Event = @event;
            return this;
        }

        #endregion

        #region Overrides of BaseJournalEntryBuilder<IEventErrorJournalEntry,EventErrorJournalEntry,IEventErrorJournalEntryBuilder>

        protected override EventErrorJournalEntry CreateEntry()
        {
            return new EventErrorJournalEntry();
        }

        protected override string GetId()
        {
            return $"{Entry.AggregateKey}_{Entry.TrackId}_event_{Entry.Event.Id}";
        }

        protected override IEventErrorJournalEntryBuilder This => this;

        #endregion
    }
}
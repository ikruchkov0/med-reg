﻿using Autofac;
using Registration.Aggregates;
using Registration.Data.Projections.Journal;
using Registration.Data.Projections.Journal.Implementations;

namespace Registration.Data.Projections
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ProjectionJournalEntryBuilder>().As<IProjectionJournalEntryBuilder>();
            builder.RegisterType<ProjectionJournal>().As<IProjectionJournal>();
            builder.RegisterType<ClinicRegistrationProjection>().As<IProjection>()
                .WithMetadata<ProjectionMetadata>(m => m
                    .For(am => am.Type, ClinicRegistrationProjection.ProjectionType)
                    .For(am => am.AggregateType, AggregateType.ClinicRegistry));
        }
    }
}
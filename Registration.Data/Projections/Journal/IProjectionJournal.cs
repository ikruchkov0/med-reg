﻿using System;
using System.Threading.Tasks;

namespace Registration.Data.Projections.Journal
{
    /// <summary>
    ///     Creates projection journal with specified name
    /// </summary>
    /// <param name="journalName"></param>
    /// <returns></returns>
    public delegate IProjectionJournal ProjectionJournalFactory(string journalName);

    public interface IProjectionJournal
    {
        Task<IProjectionJournal> EnsureJournalAsync();

        /// <summary>
        ///     Listens for events posted to journal for specified projection with trackId
        /// </summary>
        /// <param name="type"></param>
        /// <param name="trackId"></param>
        /// <returns></returns>
        IObservable<IProjectionJournalEntry> Listen(ProjectionType type, string trackId);

        /// <summary>
        ///     Writes event to projection stream with specified index and aggregate
        /// </summary>
        /// <param name="entry"></param>
        /// <returns></returns>
        Task PublishAsync(IProjectionJournalEntry entry);
    }
}
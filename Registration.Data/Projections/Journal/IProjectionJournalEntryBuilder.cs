﻿using Registration.Events;
using Registration.Journals.Implementations;

namespace Registration.Data.Projections.Journal
{
    public interface
        IProjectionJournalEntryBuilder : IJournalEntryBuilder<IProjectionJournalEntry, IProjectionJournalEntryBuilder>
    {
        IProjectionJournalEntryBuilder WithSequenceNumber(ulong sequenceNumber);
        IProjectionJournalEntryBuilder WithEvent(IEvent @event);
        IProjectionJournalEntryBuilder WithProjectionType(ProjectionType type);
    }
}
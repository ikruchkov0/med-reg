﻿using System.Threading.Tasks;
using Registration.Journals.Events;

namespace Registration.Data.Projections.Journal
{
    public interface IProjection
    {
        IProjectionJournal Journal { get; }
        Task EnsureStorageAsync();
        Task Project(IEventJournalEntry entry);
    }
}
﻿using Registration.Events;
using Registration.Journals;

namespace Registration.Data.Projections.Journal
{
    /// <summary>
    ///     Contains data for event Projection in <see cref="IProjection" />
    /// </summary>
    public interface IProjectionJournalEntry : IJournalEntry
    {
        ProjectionType Type { get; }

        ulong SequenceNumber { get; }

        /// <summary>
        ///     Payload with event data
        /// </summary>
        IEvent Event { get; }
    }
}
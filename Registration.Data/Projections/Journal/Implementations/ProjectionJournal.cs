﻿using System;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Registration.Journals.Implementations;
using Registration.RethinkDb.Common;

namespace Registration.Data.Projections.Journal.Implementations
{
    internal class ProjectionJournal : BaseJournal, IProjectionJournal
    {
        public ProjectionJournal(string journalName, IRethinkConnectionProvider connectionProvider) :
            base(journalName, "projections", connectionProvider)
        {
        }

        #region Implementation of IEventJournal

        public async Task<IProjectionJournal> EnsureJournalAsync()
        {
            await EnsureJournalTableAsync();
            return this;
        }


        public IObservable<IProjectionJournalEntry> Listen(ProjectionType type, string trackId)
        {
            return Table
                .Filter(g =>
                    g[nameof(IProjectionJournalEntry.Type)] == type.ToString() &&
                    g[nameof(IProjectionJournalEntry.TrackId)] == trackId)
                .Changes()
                .OptArg("include_initial", true)
                .RunChanges<IProjectionJournalEntry>(Connection)
                .ToObservable()
                .Select(c => c.NewValue);
        }


        public async Task PublishAsync(IProjectionJournalEntry entry)
        {
            await Table
                .Insert(entry)
                .OptArg("durability", "hard")
                .OptArg("conflict", "error")
                .RunAsync(Connection);
        }

        #endregion
    }
}
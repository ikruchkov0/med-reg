﻿using Registration.Events;
using Registration.Journals.Implementations;

namespace Registration.Data.Projections.Journal.Implementations
{
    internal sealed class ProjectionJournalEntry : BaseJournalEntry, IProjectionJournalEntry
    {
        #region Implementation of IProjectionJournalEntry

        public ProjectionType Type { get; set; }
        public ulong SequenceNumber { get; set; }
        public IEvent Event { get; set; }

        #endregion
    }
}
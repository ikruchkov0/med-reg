﻿using Registration.Events;
using Registration.Journals.Implementations;

namespace Registration.Data.Projections.Journal.Implementations
{
    internal class ProjectionJournalEntryBuilder :
        BaseJournalEntryBuilder<IProjectionJournalEntry, ProjectionJournalEntry, IProjectionJournalEntryBuilder>,
        IProjectionJournalEntryBuilder
    {
        #region Implementation of IEventJournalEntryBuilder<EventJournalEntry,out EventJournalEntryBuilder>

        public IProjectionJournalEntryBuilder WithSequenceNumber(ulong sequenceNumber)
        {
            Entry.SequenceNumber = sequenceNumber;
            return this;
        }

        public IProjectionJournalEntryBuilder WithEvent(IEvent @event)
        {
            Entry.Event = @event;
            return this;
        }

        public IProjectionJournalEntryBuilder WithProjectionType(ProjectionType type)
        {
            Entry.Type = type;
            return this;
        }

        #endregion

        #region Overrides of BaseJournalEntryBuilder<ProjectionJournalEntry, ProjectionJournalEntry, IProjectionJournalEntryBuilder>

        protected override ProjectionJournalEntry CreateEntry()
        {
            return new ProjectionJournalEntry();
        }

        protected override string GetId()
        {
            return $"{Entry.AggregateKey}_{Entry.TrackId}";
        }

        protected override IProjectionJournalEntryBuilder This => this;

        #endregion
    }
}
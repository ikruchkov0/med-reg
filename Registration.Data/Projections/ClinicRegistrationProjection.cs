﻿using System;
using System.Threading.Tasks;
using Registration.Data.Models;
using Registration.Data.Projections.Journal;
using Registration.Events.Clinics.ClinicRegistration;
using Registration.Journals.Events;
using Registration.RethinkDb.Common;
using RethinkDb.Driver;
using RethinkDb.Driver.Ast;
using RethinkDb.Driver.Net;

namespace Registration.Data.Projections
{
    public class ClinicRegistrationProjection : IProjection
    {
        private static readonly RethinkDB R = RethinkDB.R;
        private readonly IRethinkConnectionProvider _connectionProvider;
        private readonly Func<IProjectionJournalEntryBuilder> _entryBuilderFactory;

        public ClinicRegistrationProjection(IRethinkConnectionProvider connectionProvider,
            ProjectionJournalFactory journalFactory, Func<IProjectionJournalEntryBuilder> entryBuilderFactory)
        {
            _connectionProvider = connectionProvider;
            _entryBuilderFactory = entryBuilderFactory;
            Journal = journalFactory($"Projection_{ProjectionType}");
        }

        private string TableName => Constants.ClinicRegistryTable;

        private Table Table => R.Table(TableName);

        private Connection Connection => _connectionProvider.Connection;

        public static ProjectionType ProjectionType => ProjectionType.ClinicRegistry;

        public Task EnsureStorageAsync()
        {
            return Connection.EnsureTableAsync(Connection.Db, TableName);
        }

        public async Task Project(IEventJournalEntry entry)
        {
            switch (entry.Event)
            {
                case ClinicRegistered registered:

                    await Table.Insert(new ClinicDef
                    {
                        Id = registered.Id,
                        Name = registered.ClinicName,
                        Address = registered.ClinicAddress
                    }).RunAsync(Connection);
                    break;
            }
            var journalEntry = _entryBuilderFactory()
                .WithAggregateKey(entry.AggregateKey)
                .WithTrackId(entry.TrackId)
                .WithProjectionType(ProjectionType)
                .WithSequenceNumber(entry.SequenceNumber)
                .WithEvent(entry.Event)
                .ToEntry();
            await Journal.PublishAsync(journalEntry);
        }

        public IProjectionJournal Journal { get; }
    }
}
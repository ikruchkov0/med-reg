﻿using Registration.Aggregates;

namespace Registration.Data.Projections
{
    public class ProjectionMetadata
    {
        public ProjectionType Type { get; set; }

        public AggregateType AggregateType { get; set; }
    }
}
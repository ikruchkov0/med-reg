﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Registration.Data.Models
{
    public class ClinicsRegistryModel
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        public IList<ClinicDef> Clinics { get; set; }

        public ulong SequenceNumber { get; set; }
    }
}
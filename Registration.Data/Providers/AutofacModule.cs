﻿using Autofac;

namespace Registration.Data.Providers
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ClinicRegistryStorage>().AsSelf();
        }
    }
}
﻿namespace Registration.Actions.Clinics
{
    public class ClinicCommand : ICommand<IClinicAction>
    {
        public ClinicCommand(string clinicId, string trackId, IClinicAction action)
        {
            AggregateKey = clinicId;
            TrackId = trackId;
            Action = action;
        }

        #region Implementation of ICommand

        public string AggregateKey { get; }
        public string TrackId { get; }
        public IClinicAction Action { get; }

        #endregion
    }
}
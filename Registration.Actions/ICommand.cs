﻿namespace Registration.Actions
{
    public interface ICommand<out TAction>
        where TAction : IAction
    {
        string AggregateKey { get; }
        string TrackId { get; }
        TAction Action { get; }
    }

    public interface ICommand : ICommand<IAction>
    {
    }
}
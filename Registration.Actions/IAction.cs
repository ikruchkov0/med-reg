﻿namespace Registration.Actions
{
    public interface IAction
    {
        string Id { get; }
    }
}
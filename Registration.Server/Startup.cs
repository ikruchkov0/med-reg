﻿using Autofac;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Registration.Actors;
using Registration.RethinkDb.Common;
using Registration.Server.Hubs;
using AutofacModule = Registration.RethinkDb.Common.AutofacModule;

namespace Registration.Server
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSignalR();
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            var dbName = "Registration";
            var configuration = RethinkDbConfigurations.Default(dbName);
            builder.Register(c => configuration).SingleInstance();
            builder.RegisterModule<AutofacModule>();
            builder.RegisterModule<Journals.AutofacModule>();
            builder.RegisterModule<Data.Providers.AutofacModule>();
            builder.RegisterModule<Data.Projections.AutofacModule>();

            builder.RegisterModule<Aggregates.Common.AutofacModule>();
            builder.RegisterModule<Aggregates.ClinicRegistry.AutofacModule>();
            builder.RegisterModule<Actors.AutofacModule>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory,
            IDispatcher dispatcher)
        {
            loggerFactory.AddConsole(LogLevel.Debug);

            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseFileServer();

            app.UseSignalR(routes => { routes.MapHub<ClinicRegistrationHub>("clinicRegistration"); });

            dispatcher.InitAsync();
        }
    }
}
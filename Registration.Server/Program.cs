﻿using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Registration.Actors.Common;
using Registration.RethinkDb.Common;

namespace Registration.Server
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = new ContainerBuilder();
            var dbName = "Registration";
            var loggerFactory = new LoggerFactory();
            loggerFactory.AddConsole(LogLevel.Debug);

            var configuration = RethinkDbConfigurations.Default(dbName);
            builder.Register(c => loggerFactory).As<ILoggerFactory>();
            builder.Register(c => configuration).SingleInstance();

            builder.RegisterModule<AutofacModule>();
            builder.RegisterModule<Journals.AutofacModule>();
            builder.RegisterModule<Data.Projections.AutofacModule>();

            builder.RegisterModule<Aggregates.Common.AutofacModule>();
            builder.RegisterModule<Aggregates.ClinicRegistry.AutofacModule>();
            builder.RegisterModule<Actors.AutofacModule>();

            var container = builder.Build();

            var node = container.Resolve<ClusterNode>();
            node.Start("Clinics", 12001);
            node.StartProjectionAsync();
            BuildWebHost(args).Run();

            Console.ReadLine();
        }

        public static IWebHost BuildWebHost(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .ConfigureServices(services => services.AddAutofac())
                .UseStartup<Startup>()
                .UseUrls("http://localhost:5001/")
                .Build();
        }
    }
}
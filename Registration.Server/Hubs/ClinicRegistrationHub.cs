﻿using System;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Registration.Actions.Clinics.ClinicRegistration;
using Registration.Actors;
using Registration.Aggregates;
using Registration.Data.Models;
using Registration.Data.Projections;
using Registration.Data.Providers;

namespace Registration.Server.Hubs
{
    public class ClinicRegistrationHub : Hub
    {
        private readonly IDispatcher _dispatcher;
        private readonly ClinicRegistryStorage _storage;

        public ClinicRegistrationHub(IDispatcher dispatcher, ClinicRegistryStorage storage)
        {
            _dispatcher = dispatcher;
            _storage = storage;
        }

        public IObservable<ClinicDef> StreamClinics()
        {
            _storage.EnsureStorageAsync().Wait();
            return _storage.Listen().SubscribeOn(TaskPoolScheduler.Default);
        }

        public async Task AddClinic(string name)
        {
            var requestId = Guid.NewGuid().ToString();

            await _dispatcher.SendAsync(AggregateType.ClinicRegistry, new ClinicRegistrationCommand(requestId,
                new RegisterClinic
                {
                    ClinicName = name,
                    ClinicAddress = "123",
                    Id = Guid.NewGuid().ToString()
                }), ProjectionType.ClinicRegistry);
        }
    }
}
using AutoMapper;
using Registration.Actions;
using Registration.Actions.Clinics;
using Registration.Events.Clinics;

namespace Registration.Events.Mappings
{
    internal class ActionMapper : IActionMapper
    {
        private readonly IMapper _mapper;

        public ActionMapper()
        {
            var configure = new MapperConfiguration(Configure);
            _mapper = new Mapper(configure);
        }

        public TEvent Map<TEvent>(IAction action)
            where TEvent : class, IEvent
        {
            return _mapper.Map<TEvent>(action);
        }

        private static void Configure(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<BaseAction, BaseEvent>();

            cfg.CreateMap<CreateClinic, ClinicCreated>().IncludeBase<BaseAction, BaseEvent>();
        }
    }
}
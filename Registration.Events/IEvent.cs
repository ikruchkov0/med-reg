﻿namespace Registration.Events
{
    public interface IEvent
    {
        string Id { get; }
    }
}
﻿using System;

namespace Registration.Events
{
    public abstract class BaseEvent : IEvent
    {
        protected BaseEvent()
        {
            Id = Guid.NewGuid().ToString();
        }

        public string Id { get; set; }
    }
}